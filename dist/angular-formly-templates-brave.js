//! angular-formly-templates-brave version 0.0.7 built with ♥ by Sizeof (ó ì_í)=óò=(ì_í ò)

(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("angular"), require("angular-formly"), require("api-check"));
	else if(typeof define === 'function' && define.amd)
		define(["angular", "angular-formly", "api-check"], factory);
	else if(typeof exports === 'object')
		exports["ngFormlyTemplatesBrave"] = factory(require("angular"), require("angular-formly"), require("api-check"));
	else
		root["ngFormlyTemplatesBrave"] = factory(root["angular"], root["ngFormly"], root["apiCheck"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_3__, __WEBPACK_EXTERNAL_MODULE_4__, __WEBPACK_EXTERNAL_MODULE_5__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	module.exports = __webpack_require__(1);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});
	var ngModuleName = 'formlyBrave';
	var angular = __webpack_require__(2);
	var ngModule = angular.module(ngModuleName, [__webpack_require__(4)]);
	ngModule.constant('formlyBraveApiCheck', __webpack_require__(5)({
	  output: {
	    prefix: 'angular-formly-brave'
	  }
	}));
	ngModule.constant('formlyBraveVersion', ("0.0.7"));

	__webpack_require__(6)(ngModule);
	__webpack_require__(9)(ngModule);
	__webpack_require__(25)(ngModule);

	exports['default'] = ngModuleName;
	module.exports = exports['default'];

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	// some versions of angular don't export the angular module properly,
	// so we get it from window in this case.
	'use strict';

	var angular = __webpack_require__(3);
	if (!angular.version) {
	  angular = window.angular;
	}
	module.exports = angular;

/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_3__;

/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	exports['default'] = function (ngModule) {
	  ngModule.config(addWrappers);

	  function addWrappers(formlyConfigProvider) {
	    formlyConfigProvider.setWrapper([{
	      name: 'bootstrapLabel',
	      template: __webpack_require__(7),
	      apiCheck: function apiCheck(check) {
	        return {
	          templateOptions: {
	            label: check.string.optional,
	            required: check.bool.optional,
	            labelSrOnly: check.bool.optional,
	            sectionClass: check.string.optional
	          }
	        };
	      }
	    }, { name: 'bootstrapHasError', template: __webpack_require__(8) }]);
	  }
	  addWrappers.$inject = ["formlyConfigProvider"];
	};

	module.exports = exports['default'];

/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = "<section class=\"{{ to.sectionClass }}\">\n  <label for=\"{{id}}\" class=\"label {{to.labelSrOnly ? 'sr-only' : ''}}\" ng-if=\"to.label\">\n    {{to.label}}\n    {{to.required ? '*' : ''}}\n  </label>\n  <formly-transclude></formly-transclude>\n</section>\n"

/***/ },
/* 8 */
/***/ function(module, exports) {

	module.exports = "<formly-transclude></formly-transclude>\n<div ng-messages=\"fc.$error\" ng-if=\"(form.$submitted || options.formControl.$touched) && options.formControl.$invalid\" class=\"note note-error darkred\">\n    <span ng-message=\"{{ ::name }}\" ng-repeat=\"(name, message) in ::options.validation.messages\">\n        {{ message(fc.$viewValue, fc.$modelValue, this) }}\n    </span>\n</div>\n"

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	exports['default'] = function (ngModule) {
	  __webpack_require__(10)(ngModule);
	  __webpack_require__(12)(ngModule);
	  __webpack_require__(14)(ngModule);
	  __webpack_require__(16)(ngModule);
	  __webpack_require__(17)(ngModule);
	  __webpack_require__(19)(ngModule);
	  __webpack_require__(20)(ngModule);
	  __webpack_require__(21)(ngModule);
	  __webpack_require__(23)(ngModule);
	};

	module.exports = exports['default'];

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	exports['default'] = function (ngModule) {
	  ngModule.config(addCheckboxType);

	  function addCheckboxType(formlyConfigProvider) {
	    formlyConfigProvider.setType({
	      name: 'checkbox',
	      template: __webpack_require__(11),
	      apiCheck: function apiCheck(check) {
	        return {
	          templateOptions: {
	            label: check.string
	          }
	        };
	      }
	    });
	  }
	  addCheckboxType.$inject = ["formlyConfigProvider"];
	};

	module.exports = exports['default'];

/***/ },
/* 11 */
/***/ function(module, exports) {

	module.exports = "<section>\n<label class=\"checkbox\">\n\t<input type=\"checkbox\"\n\t   class=\"formly-field-checkbox\"\n\t\t   ng-model=\"model[options.key]\">\n\t<i></i> {{to.label}}\n\t{{to.required ? '*' : ''}}\n</label>\n</section>"

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	exports['default'] = function (ngModule) {
	  ngModule.config(addCheckboxType);

	  function addCheckboxType(formlyConfigProvider) {
	    formlyConfigProvider.setType({
	      name: 'multiCheckbox',
	      template: __webpack_require__(13),
	      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
	      apiCheck: function apiCheck(check) {
	        return {
	          templateOptions: {
	            options: check.arrayOf(check.object),
	            labelProp: check.string.optional,
	            valueProp: check.string.optional
	          }
	        };
	      },
	      defaultOptions: {
	        noFormControl: false,
	        ngModelAttrs: {
	          required: {
	            attribute: '',
	            bound: ''
	          }
	        }
	      },
	      controller: /* @ngInject */["$scope", function controller($scope) {
	        var to = $scope.to;
	        var opts = $scope.options;
	        $scope.multiCheckbox = {
	          checked: [],
	          change: setModel
	        };

	        // initialize the checkboxes check property
	        $scope.$watch('model', function modelWatcher(newModelValue) {
	          var modelValue, valueProp;

	          if (Object.keys(newModelValue).length) {
	            modelValue = newModelValue[opts.key];

	            $scope.$watch('to.options', function optionsWatcher(newOptionsValues) {
	              if (newOptionsValues && Array.isArray(newOptionsValues) && Array.isArray(modelValue)) {
	                valueProp = to.valueProp || 'value';
	                for (var index = 0; index < newOptionsValues.length; index++) {
	                  $scope.multiCheckbox.checked[index] = modelValue.indexOf(newOptionsValues[index][valueProp]) !== -1;
	                }
	              }
	            });
	          }
	        }, true);

	        function checkValidity(expressionValue) {
	          var valid;

	          if ($scope.to.required) {
	            valid = angular.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0 && expressionValue;

	            $scope.fc.$setValidity('required', valid);
	          }
	        }

	        function setModel() {
	          $scope.model[opts.key] = [];
	          angular.forEach($scope.multiCheckbox.checked, function (checkbox, index) {
	            if (checkbox) {
	              $scope.model[opts.key].push(to.options[index][to.valueProp || 'value']);
	            }
	          });

	          // Must make sure we mark as touched because only the last checkbox due to a bug in angular.
	          $scope.fc.$setTouched();
	          checkValidity(true);

	          if ($scope.to.onChange) {
	            $scope.to.onChange();
	          }
	        }

	        if (opts.expressionProperties && opts.expressionProperties['templateOptions.required']) {
	          $scope.$watch(function () {
	            return $scope.to.required;
	          }, function (newValue) {
	            checkValidity(newValue);
	          });
	        }

	        if ($scope.to.required) {
	          var unwatchFormControl = $scope.$watch('fc', function (newValue) {
	            if (!newValue) {
	              return;
	            }
	            checkValidity(true);
	            unwatchFormControl();
	          });
	        }
	      }]
	    });
	  }
	  addCheckboxType.$inject = ["formlyConfigProvider"];
	};

	module.exports = exports['default'];

/***/ },
/* 13 */
/***/ function(module, exports) {

	module.exports = "<div class=\"radio-group\">\n  <div ng-repeat=\"(key, option) in to.options\" class=\"checkbox\">\n    <label>\n      <input type=\"checkbox\"\n             id=\"{{id + '_'+ $index}}\"\n             ng-model=\"multiCheckbox.checked[$index]\"\n             ng-change=\"multiCheckbox.change()\">\n      {{option[to.labelProp || 'name']}}\n    </label>\n  </div>\n</div>\n"

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	exports['default'] = function (ngModule) {
	  ngModule.config(addMatchInputType);

	  function addMatchInputType(formlyConfigProvider) {
	    formlyConfigProvider.setType({
	      name: 'matchField',
	      template: __webpack_require__(15),
	      apiCheck: function apiCheck() {
	        return {
	          data: {
	            fieldToMatch: formlyExampleApiCheck.string
	          }
	        };
	      },
	      apiCheckOptions: {
	        prefix: 'matchField type'
	      },
	      defaultOptions: function matchFieldDefaultOptions(options) {
	        return {
	          extras: {
	            validateOnModelChange: true
	          },
	          expressionProperties: {
	            'templateOptions.disabled': function templateOptionsDisabled(viewValue, modelValue, scope) {
	              var matchField = find(scope.fields, 'key', options.data.fieldToMatch);
	              if (!matchField) {
	                throw new Error('Could not find a field for the key ' + options.data.fieldToMatch);
	              }
	              var model = options.data.modelToMatch || scope.model;
	              var originalValue = model[options.data.fieldToMatch];
	              var invalidOriginal = matchField.formControl && matchField.formControl.$invalid;
	              return !originalValue || invalidOriginal;
	            }
	          },
	          validators: {
	            fieldMatch: {
	              expression: function expression(viewValue, modelValue, fieldScope) {
	                var value = modelValue || viewValue;
	                var model = options.data.modelToMatch || fieldScope.model;
	                return value === model[options.data.fieldToMatch];
	              },
	              message: options.data.matchFieldMessage || '"Must match"'
	            }
	          }
	        };

	        function find(array, prop, value) {
	          var foundItem;
	          array.some(function (item) {
	            if (item[prop] === value) {
	              foundItem = item;
	            }
	            return !!foundItem;
	          });
	          return foundItem;
	        }
	      },
	      wrapper: ['bootstrapHasError', 'bootstrapLabel']
	    });
	  }
	  addMatchInputType.$inject = ["formlyConfigProvider"];
	};

	module.exports = exports['default'];

/***/ },
/* 15 */
/***/ function(module, exports) {

	module.exports = "<label class=\"input\" ng-class=\"{ 'state-error': showError }\">\n    <i class=\"icon-append fa fa-question-circle\"></i>\n    <input ng-model=\"model[options.key]\">\n    <b class=\"tooltip tooltip-top-right\">\n        <i class=\"fa fa-warning txt-color-teal\"></i>\n        {{ to.helpText }}\n    </b>\n</label>"

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	exports['default'] = function (ngModule) {
	  ngModule.config(addInputType);

	  function addInputType(formlyConfigProvider) {
	    formlyConfigProvider.setType({
	      name: 'input',
	      template: __webpack_require__(15),
	      apiCheck: function apiCheck(check) {
	        return {
	          templateOptions: {
	            helpText: check.string
	          }
	        };
	      },
	      wrapper: ['bootstrapHasError', 'bootstrapLabel']
	    });
	  }
	  addInputType.$inject = ["formlyConfigProvider"];
	};

	module.exports = exports['default'];

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	exports['default'] = function (ngModule) {
	  ngModule.config(addRadioType);

	  function addRadioType(formlyConfigProvider) {
	    formlyConfigProvider.setType({
	      name: 'radio',
	      template: __webpack_require__(18),
	      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
	      defaultOptions: {
	        noFormControl: false
	      },
	      apiCheck: function apiCheck(check) {
	        return {
	          templateOptions: {
	            options: check.arrayOf(check.object),
	            labelProp: check.string.optional,
	            valueProp: check.string.optional,
	            inline: check.bool.optional
	          }
	        };
	      }
	    });
	  }
	  addRadioType.$inject = ["formlyConfigProvider"];
	};

	module.exports = exports['default'];

/***/ },
/* 18 */
/***/ function(module, exports) {

	module.exports = "<div class=\"radio-group\">\n  <div ng-repeat=\"(key, option) in to.options\" ng-class=\"{ 'radio': !to.inline, 'radio-inline': to.inline }\">\n    <label>\n      <input type=\"radio\"\n             id=\"{{id + '_'+ $index}}\"\n             tabindex=\"0\"\n             ng-value=\"option[to.valueProp || 'value']\"\n             ng-model=\"model[options.key]\">\n      {{option[to.labelProp || 'name']}}\n    </label>\n  </div>\n</div>\n"

/***/ },
/* 19 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

	exports['default'] = function (ngModule) {
	  ngModule.config(addSelectType);

	  var template = '<label class="select" ng-class="{ \'state-error\': showError }"><select ng-model="model[options.key]"></select><i></i></label>';

	  function addSelectType(formlyConfigProvider) {
	    formlyConfigProvider.setType({
	      name: 'select',
	      template: template,
	      wrapper: ['bootstrapHasError', 'bootstrapLabel'],
	      defaultOptions: function defaultOptions(options) {
	        /* jshint maxlen:195 */
	        var ngOptions = options.templateOptions.ngOptions || 'option[to.valueProp || \'value\'] as option[to.labelProp || \'name\'] group by option[to.groupProp || \'group\'] for option in to.options';
	        return {
	          ngModelAttrs: _defineProperty({}, ngOptions, {
	            value: options.templateOptions.optionsAttr || 'ng-options'
	          })
	        };
	      },
	      apiCheck: function apiCheck(check) {
	        return {
	          templateOptions: {
	            options: check.arrayOf(check.object),
	            optionsAttr: check.string.optional,
	            labelProp: check.string.optional,
	            valueProp: check.string.optional,
	            groupProp: check.string.optional
	          }
	        };
	      }
	    });
	  }
	  addSelectType.$inject = ["formlyConfigProvider"];
	};

	module.exports = exports['default'];

/***/ },
/* 20 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	exports['default'] = function (ngModule) {
	  ngModule.config(addTextareaType);

	  function addTextareaType(formlyConfigProvider) {
	    formlyConfigProvider.setType({
	      name: 'textarea',
	      template: '<label class="textarea"><textarea ng-model="model[options.key]"></textarea></label>',
	      wrapper: ['bootstrapLabel'],
	      defaultOptions: {
	        ngModelAttrs: {
	          rows: { attribute: 'rows' },
	          cols: { attribute: 'cols' }
	        }
	      },
	      apiCheck: function apiCheck(check) {
	        return {
	          templateOptions: {
	            rows: check.number.optional,
	            cols: check.number.optional
	          }
	        };
	      }
	    });
	  }
	  addTextareaType.$inject = ["formlyConfigProvider"];
	};

	module.exports = exports['default'];

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	exports['default'] = function (ngModule) {
	  ngModule.config(addFileSelectorType);

	  function addFileSelectorType(formlyConfigProvider) {
	    formlyConfigProvider.setType({
	      name: 'fileselector',
	      template: __webpack_require__(22),
	      wrapper: ['bootstrapLabel']
	    });
	  }
	  addFileSelectorType.$inject = ["formlyConfigProvider"];
	};

	module.exports = exports['default'];

/***/ },
/* 22 */
/***/ function(module, exports) {

	module.exports = "<div class=\"input input-file\">\n    <span class=\"button\" ng-click=\"openTinyBrowse('destination','image')\">{{ \"Browse\" | translate }}</span>\n    <!--<span class=\"button\" ng-click=\"clearSelection('destination')\"><i class=\"glyphicon glyphicon-remove\"></i></span>-->\n    <input placeholder=\"{{ 'Choose file' | translate }}\" name=\"model[options.key]\"\n           id=\"destinationInput\" type=\"text\" ng-click=\"openTinyBrowse('destination','image')\">\n</div>\n\n\n<div class=\"img-preview margin-top ng-hide well col col-6\" id=\"destinationImg\">\n    <div class=\"preview-text\">{{\"Preview\" | translate }}</div>\n</div>\n<div class=\"clearfix\"></div>\n\n\n<div id=\"tinyCtrl\" ng-app=\"app.files\" ng-controller=\"FilesSelectorController as tiny\">\n    <div class=\"tb\" ng-if=\"browserOn\">\n        <div class=\"tb-container\">\n            <div class=\"tb-frame\">\n                <div class=\"tb-toolbar\">\n                    <div class=\"tb-heading\">Browse - {{getHeading()}}</div>\n                    <div class=\"tb-right\">\n                        <div class=\"tb-views\">\n                            <button class=\"tb-btn\" ng-click=\"changeView('grid')\"><icon>&#xf00a;</icon></button>\n                            <button class=\"tb-btn\" ng-click=\"changeView('row')\"><icon>&#xf0c9;</icon></button>\n                        </div>\n                        <div class=\"tb-search\">\n\n                            <input id=\"tb-input\" formnovalidate=\"formnovalidate\" placeholder=\"search\" type=\"text\" ng-model=\"$parent.searchTerm\" ng-keyup=\"search()\"/>\n                            <div class=\"tb-clear\" ng-click=\"searchClear()\"><icon>&#xf00d;</icon></div>\n                        </div>\n                        <div class=\"tb-btn tb-close\" ng-click=\"close()\"><icon>&#xf00d;</icon></div>\n                    </div>\n                </div>\n                <div class=\"tb-window\">\n                    <div class=\"tb-scroll\">\n                        <div class=\"tb-images\">\n\n                            <!-- FOLDERS -->\n                            <div class=\"tb-image {{view}}\" ng-click=\"openFolder(folder.sysName)\" ng-repeat=\"folder in folders\">\n                                <div class=\"tb-image-frame tb-folder\">\n                                    <span ng-bind-html=\"getIcon(folder.type)\"></span>\n                                </div><div class=\"tb-image-name\">{{folder.name}}</div>\n                            </div>\n                            <!-- /FOLDERS -->\n\n                            <!-- IMAGES -->\n                            <div class=\"tb-image {{view}}\" ng-click=\"selectItem($index)\" ng-repeat=\"item in items\">\n\n                                <!-- IMAGE PREVIEW -->\n                                <div class=\"tb-image-frame\" style=\"background:url('{{item.img}}')\" ng-if=\"browseType == 'image'\">\n                                    <icon ng-bind-html=\"getIcon(item.type)\"></icon>\n                                </div>\n                                <!-- IMAGE PREVIEW -->\n\n                                <!-- FILE PREVIEW -->\n                                <div class=\"tb-image-frame tb-file-frame\" ng-if=\"browseType == 'file'\">\n                                    <icon ng-bind-html=\"getExtIcon(item.name)\"></icon>\n                                </div>\n                                <!-- FILE PREVIEW -->\n\n\n                                <div class=\"tb-image-name\">{{item.name}}</div>\n                            </div>\n                            <!-- /IMAGES -->\n\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	exports['default'] = function (ngModule) {
	  ngModule.config(addUiSelectType);

	  function addUiSelectType(formlyConfigProvider) {
	    formlyConfigProvider.setType({
	      name: 'ui-select',
	      'extends': 'select',
	      template: __webpack_require__(24),
	      wrapper: ['bootstrapHasError', 'bootstrapLabel']
	    });
	  }
	  addUiSelectType.$inject = ["formlyConfigProvider"];
	};

	module.exports = exports['default'];

/***/ },
/* 24 */
/***/ function(module, exports) {

	module.exports = "<ui-select data-ng-model=\"model[options.key]\" data-required=\"{{to.required}}\" data-disabled=\"{{to.disabled}}\" theme=\"bootstrap\">\n    <ui-select-match placeholder=\"{{to.placeholder}}\">{{$select.selected[to.labelProp]}} <span ng-show=\"to.loading\"><i class=\"fa fa-cog fa-spin fa-fw\"></i> Loading...</span></ui-select-match>\n    <ui-select-choices data-repeat=\"option[to.valueProp] as option in to.options | filter: $select.search\">\n        <div ng-bind-html=\"option[to.labelProp] | highlight: $select.search\"></div>\n    </ui-select-choices>\n</ui-select>"

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	var _addons = __webpack_require__(26);

	var _addons2 = _interopRequireDefault(_addons);

	var _description = __webpack_require__(28);

	var _description2 = _interopRequireDefault(_description);

	exports['default'] = function (ngModule) {
	  (0, _addons2['default'])(ngModule);
	  (0, _description2['default'])(ngModule);
	};

	module.exports = exports['default'];

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	exports['default'] = function (ngModule) {
	  ngModule.run(addAddonsManipulator);

	  function addAddonsManipulator(formlyConfig, formlyBraveApiCheck) {
	    var addonTemplate = __webpack_require__(27);
	    var addonChecker = formlyBraveApiCheck.shape({
	      'class': formlyBraveApiCheck.string.optional,
	      text: formlyBraveApiCheck.string.optional,
	      onClick: formlyBraveApiCheck.func.optional
	    }).strict.optional;
	    var api = formlyBraveApiCheck.shape({
	      templateOptions: formlyBraveApiCheck.shape({
	        addonLeft: addonChecker,
	        addonRight: addonChecker
	      })
	    });
	    formlyConfig.templateManipulators.preWrapper.push(function (template, options) {
	      if (!options.templateOptions.addonLeft && !options.templateOptions.addonRight) {
	        return template;
	      }
	      formlyBraveApiCheck.warn([api], [options]);
	      return addonTemplate.replace('<formly-transclude></formly-transclude>', template);
	    });
	  }
	  addAddonsManipulator.$inject = ["formlyConfig", "formlyBraveApiCheck"];
	};

	module.exports = exports['default'];

/***/ },
/* 27 */
/***/ function(module, exports) {

	module.exports = "<div ng-class=\"{'input-group': to.addonLeft || to.addonRight}\">\n    <div class=\"input-group-addon\"\n         ng-if=\"to.addonLeft\"\n         ng-style=\"{cursor: to.addonLeft.onClick ? 'pointer' : 'inherit'}\"\n         ng-click=\"to.addonLeft.onClick(options, this, $event)\">\n        <i class=\"{{to.addonLeft.class}}\" ng-if=\"to.addonLeft.class\"></i>\n        <span ng-if=\"to.addonLeft.text\">{{to.addonLeft.text}}</span>\n    </div>\n    <formly-transclude></formly-transclude>\n    <div class=\"input-group-addon\"\n         ng-if=\"to.addonRight\"\n         ng-style=\"{cursor: to.addonRight.onClick ? 'pointer' : 'inherit'}\"\n         ng-click=\"to.addonRight.onClick(options, this, $event)\">\n        <i class=\"{{to.addonRight.class}}\" ng-if=\"to.addonRight.class\"></i>\n        <span ng-if=\"to.addonRight.text\">{{to.addonRight.text}}</span>\n    </div>\n</div>\n"

/***/ },
/* 28 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	exports['default'] = function (ngModule) {
	  ngModule.run(addDescriptionManipulator);

	  function addDescriptionManipulator(formlyConfig) {
	    formlyConfig.templateManipulators.preWrapper.push(function ariaDescribedBy(template, options, scope) {
	      if (angular.isDefined(options.templateOptions.description)) {
	        var el = document.createElement('div');
	        el.appendChild(angular.element(template)[0]);
	        el.appendChild(angular.element('<p id="' + scope.id + '_description"' + 'class="help-block"' + 'ng-if="to.description">' + '{{to.description}}' + '</p>')[0]);
	        var modelEls = angular.element(el.querySelectorAll('[ng-model]'));
	        if (modelEls) {
	          modelEls.attr('aria-describedby', scope.id + '_description');
	        }
	        return el.innerHTML;
	      } else {
	        return template;
	      }
	    });
	  }
	  addDescriptionManipulator.$inject = ["formlyConfig"];
	};

	module.exports = exports['default'];

/***/ }
/******/ ])
});
;