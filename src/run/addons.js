export default ngModule => {
  ngModule.run(addAddonsManipulator);

  function addAddonsManipulator(formlyConfig, formlyBraveApiCheck) {
    var addonTemplate = require('./addons.html');
    const addonChecker = formlyBraveApiCheck.shape({
      class: formlyBraveApiCheck.string.optional,
      text: formlyBraveApiCheck.string.optional,
      onClick: formlyBraveApiCheck.func.optional
    }).strict.optional;
    const api = formlyBraveApiCheck.shape({
      templateOptions: formlyBraveApiCheck.shape({
        addonLeft: addonChecker,
        addonRight: addonChecker
      })
    });
    formlyConfig.templateManipulators.preWrapper.push(function(template, options) {
      if (!options.templateOptions.addonLeft && !options.templateOptions.addonRight) {
        return template;
      }
      formlyBraveApiCheck.warn([api], [options]);
      return addonTemplate.replace('<formly-transclude></formly-transclude>', template);
    });
  }
};
