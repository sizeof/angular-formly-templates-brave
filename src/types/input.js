export default ngModule => {
  ngModule.config(addInputType);

  function addInputType(formlyConfigProvider) {
    formlyConfigProvider.setType({
      name: 'input',
      template: require('./input.html'),
      apiCheck: check => ({
          templateOptions: {
            helpText: check.string
          }
      }),
      wrapper: ['bootstrapHasError', 'bootstrapLabel']
    });
  }
};
