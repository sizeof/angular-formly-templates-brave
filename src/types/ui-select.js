export default  ngModule => {
  ngModule.config(addUiSelectType);


  function addUiSelectType(formlyConfigProvider) {
    formlyConfigProvider.setType({
      name: 'ui-select',
      extends: 'select',
      template: require('./ui-select.html'),
      wrapper: ['bootstrapHasError', 'bootstrapLabel']
    });
  }
};
