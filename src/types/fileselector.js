export default ngModule => {
  ngModule.config(addFileSelectorType);

  function addFileSelectorType(formlyConfigProvider) {
    formlyConfigProvider.setType({
      name: 'fileselector',
      template: require('./fileselector.html'),
      wrapper: ['bootstrapLabel']
    });
  }
};
