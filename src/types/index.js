export default ngModule => {
  require('./checkbox')(ngModule);
  require('./multi-checkbox')(ngModule);
  require('./match-input')(ngModule);
  require('./input')(ngModule);
  require('./radio')(ngModule);
  require('./select')(ngModule);
  require('./textarea')(ngModule);
  require('./fileselector')(ngModule);
  require('./ui-select')(ngModule);
};
