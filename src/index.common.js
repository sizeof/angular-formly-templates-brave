const ngModuleName = 'formlyBrave';
const angular = require('./angular-fix');
const ngModule = angular.module(ngModuleName, [require('angular-formly')]);
ngModule.constant(
  'formlyBraveApiCheck',
  require('api-check')({
    output: {
      prefix: 'angular-formly-brave'
    }
  })
);
ngModule.constant('formlyBraveVersion', VERSION);

require('./wrappers')(ngModule);
require('./types')(ngModule);
require('./run')(ngModule);

export default ngModuleName;
